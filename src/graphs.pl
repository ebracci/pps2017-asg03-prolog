% fromList(+List,-Graph)
% fromList([10,20,30],[e(10,20),e(20,30)]).
% fromList([10,20],[e(10,20)]).
% fromList([10],[]).

fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% fromCircList(+List,-Graph)
%fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
% fromCircList([10,20],[e(10,20),e(20,10)]).
% fromCircList([10],[e(10,10)]).

fromCircList([H1],[e(H1,H)], H).
fromCircList([H1|T], Nodes) :- fromCircList([H1|T],Nodes, H1).
fromCircList([H1,H2|T],[e(H1,H2)|L], H) :- fromCircList([H2|T], L, H).  

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1

dropNode(G,N,O):- dropAll(G,e(N,_),G2),
dropAll(G2,e(_,N),O).

% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)
% reaching([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2,3]
% reaching([e(1,2),e(1,2),e(2,3)],1,L). -> L/[2,2])

reaching(Graph, Node, List) :- findall(X, member(e(Node,X), Graph), List). 

% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
% anypath([e(1,2),e(1,3),e(2,3)],1,3,L).

anypath(Graph, Node1, Node2, [e(Node1,Node2)]) :- member(e(Node1,Node2), Graph).
anypath(Graph, Node1, Node2, List) :- member(e(Node1, Node3), Graph), anypath(Graph, Node3, Node2, List).

% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
% allreaching([e(1,2),e(2,3),e(3,5)],1,[2,3,5]).

allreaching(Graph, Node, List) :- findall(X, anypath(Graph, Node, X, _), List).

