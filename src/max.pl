% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element

max([H|T],Max) :- max(T,Max,H), !.
max([H|T],Max,TempMax) :- H=<TempMax, max(T,Max,TempMax).
max([H|T],Max,TempMax) :- H>TempMax, max(T,Max,H).
max([],Max, Max).
