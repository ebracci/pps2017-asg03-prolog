% size(List,Size)
% Size will contain the number of elements in List

size([],zero).
size([_|T],s(M)) :- size(T,M).

% yes, it allows a pure relational behaviour