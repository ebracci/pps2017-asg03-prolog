% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
% last([1,2,3],5,[1,2,3,5]).

last([],X,[X]).
last([H|T], Y, [H|Xs]) :- last(T, Y, Xs).

seqR2(N, N, [N]).
seqR2(N, [H|T]) :- seqR2(N, 0, [H|T]).
seqR2(N, M, [M|T]) :- M =< N, M2 is M + 1, seqR2(N,M2, T).

