% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).

times(L, N, L1):- times(L, N, L1, R).
times(L, N, L1, L1) :- !.
times(L, N, L1, R) :- N > 0, N1 is N - 1, append(L,L,R), times(L,N1,L1,R).