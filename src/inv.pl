% inv(List,List)
% example: inv([1,2,3],[3,2,1]).

inv([H|T],A,R):-  inv(T,[H|A],R).
inv([],A,A).
inv(L,R):-  inv(L,[],R).