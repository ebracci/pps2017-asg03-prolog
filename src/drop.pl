% dropAny(Elem,List,OutList).
% dropAny(10,[10,20,10,30,10],L).

dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% dropFirst: drops only the first occurrence (showing no alternative results)
dropFirst(X, [X|T], T) :- !.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

% dropLast: drops only the last occurrence (showing no alternative results)
dropLast(X, List, R):- reverse(List, ListR), dropFirst(X, ListR, R1), reverse(R1, R).

% dropAll:  drop all occurrences, returning a single list as result
dropAll(_, [], []).
dropAll(X, [Y|Ys], R) :- X=Y, dropAll(X, Ys,R), !. 
dropAll(X, [Y|Ys], [Y|T]) :- dropAll(X, Ys,T). 