% search_two(Elem,List)
% looks for two occurences of Elem with an element in between!

search_two(X, [X,Y,X|_]):-!.
search_two(X, [_|T]) :- search_two(X,T).

% yes, search_two is fully relational